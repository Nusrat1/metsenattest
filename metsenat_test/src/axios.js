import axios from 'axios'
import { useDefaultStore } from './store'
const baseURL = import.meta.env.VITE_SOME_KEY
// axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('access_token_metsenat')}`
const instance =  axios.create({
  baseURL
})
instance.interceptors.response.use(function (response) {
    return response
  }, function (error) {
    if (error.response.status === 401) {
       useDefaultStore().logOut()
      //  Yoki refresh tokenga request
    }
    return Promise.reject(error)
  })
  export default instance

