import { defineStore } from 'pinia';
import axios from '../axios'
import router from '../router';
export const useDefaultStore = defineStore('default', {
  state: () => ({ sponsors: [], count: null }),
  getters: {
   getSponsorsList(){
    return this.sponsors.results.map((list, key) => ({ ...list, key }))
   }
  },
  actions: {
    login(payload) {
      return new Promise((resolve, reject) => {
        axios.post('/auth/login/', payload)
          .then(response => {
            if (response) {
              console.log(response);
              const token = response.data.access
              localStorage.setItem('access_token_metsenat', token)
              router.push('/')
              resolve(response)
            } else {
              reject({ message: 'Wrong Login or Password' })
            }
          })
          .catch(error => { reject(error) })
      })
    },
    logOut() {
      localStorage.removeItem('access_token_metsenat')
      router.push('/login')
    },
    async GetSponsors(payload) {
      const res = await axios.get('/sponsor-list/', { params: { ...payload } })
      console.log(res);
      this.sponsors = res.data
    }
  },
})