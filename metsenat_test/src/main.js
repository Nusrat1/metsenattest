import { createApp } from 'vue'
import App from './App.vue'
import router from './router.js'
import { createPinia } from 'pinia'
import naive from 'naive-ui'
// ------ Styles import -------
import './assets/styles/main.scss'
const pinia = createPinia()
createApp(App).use(router).use(pinia).use(naive).mount('#app')
