import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: "/",
        redirect: '/home',
    },
    {
        path: '/',
        name: 'Default Layout',
        component: () => import("./layouts/Default.vue"),
        children: [
            {
                path: '/home',
                name: 'Home page',
                component: () => import("./views/Home.vue"), 
            }
        ]
    },
    {
        path: '/',
        name: 'Full Layout',
        component: () => import("./layouts/fullPage.vue"),
        children: [
            {
                path: '/login',
                name: 'Login page',
                component: () => import("./views/login.vue"), 
            }
        ]
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
    linkActiveClass: "active",
})
// path 
router.beforeEach((to, from, next) => {
    const publicPages = ["/login"];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem("access_token_metsenat");
    if (authRequired && !loggedIn) {
      !publicPages;
      return next("/login");
    }
    else {
      return next();
    }
  });

export default router
